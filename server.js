const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const socket = require('socket.io');
const app = express();
 
const compiler = webpack(webpackConfig);
 
app.use(express.static(__dirname + '/www'));
 
app.use(webpackDevMiddleware(compiler, {
  hot: true,
  filename: 'bundle.js',
  publicPath: '/',
  stats: {
    colors: true,
  },
  historyApiFallback: true,
}));


//const wf = new ws('http://localhost:4000/#/?_k=6qxccg');

//wf.on('open', () => w.send(msg))


const server = app.listen(process.env.PORT || 4000, function() {
  const host = server.address().address;
  const port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

let io = socket(server)
io.on('connection',(client) => {
  client.on('subscribeToTimer', (interval) => {
    console.log('client is subscribing to timer with interval ', interval);
  });
  setInterval(() => {
    client.emit('timer', new Date());
  }, interval);
  
  console.log('made client connectionzzzzzzzzzz::::::::::',client.id)
})


const ws = require('ws')
const w = new ws('wss://api.bitfinex.com/ws/2')

w.on('message', (exchangeData) => {
  var jsonData = JSON.parse(exchangeData)
   if(Array.isArray(jsonData[1])){
    io.sockets.emit('coinData', jsonData[1][0])
    console.log('hit',jsonData[1],jsonData[1][0])
   }
  })

let msg = JSON.stringify({ 
  event: 'subscribe', 
  channel: 'ticker', 
  symbol: 'tBTCUSD' 
})

w.on('open', () => w.send(msg))