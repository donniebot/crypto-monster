import React from 'react';
import { Link } from "react-router";


class Layout extends React.Component {

  render() {

    return (
      <div className="layout">
          {this.props.children}
      </div>
    );
  }
}
export default Layout;
//     <img className="logo" src="images/logo.svg" alt="donno-robot"/>