import React from 'react'

export default (props) => {
  const {currentTodo, updateCurrent, addTodo, todos} = props
  let val = ''
  const handleInputChange = (evt) => {
    val = evt.target.value
    updateCurrent(val)
  }
  const handleSubmit = (evt) => {
    const payload = {id:todos.length+1, name: currentTodo, isComplete: true}
    addTodo(payload)
    evt.preventDefault()
  }
  return (
    <form onSubmit={handleSubmit}>
      <input id="theInput" type="text"
        onChange={handleInputChange}
        value={currentTodo}/>
      <input type="submit" value="Submit" />
    </form>
  )
}
