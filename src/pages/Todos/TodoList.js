import React from 'react'
let crossClass = ''
const TodoItem = ({id, name, isComplete}) => {
  const crossOut = () => {
    if(crossClass === ''){
      crossClass = 'cross-out'
    }else{
      crossClass = ''
    }
    console.log(crossClass)
  }
  
  return (
    <li>
      <input type="checkbox" defaultChecked={isComplete} />
      <span onClick={crossOut} className={crossClass}>{name}</span>
    </li>
  )
}

export default (props) => (
  <div className="Todo-List">
    <ul>
      {props.todos.map(todo => <TodoItem key={todo.id} {...todo} />)}
    </ul>
  </div>
)
