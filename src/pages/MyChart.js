import React from 'react';
import { Link } from 'react-router';
import Chart from 'c3js-react'
import ioClient from 'socket.io-client'

let io = ioClient('http://localhost:4000')
let data = {}
console.log("socket:::" ,io)
io.on('coinData', function(data){
  console.log("heer",data)
  data = {
    columns: [
        ["brazil", data],
        ["france", 30],
        ["united_states", 90],
      ],
    type: 'donut',
    colors: {
        brazil: 'red',
        france: 'green',
        united_states: 'pink',
    }
  };
})

data = {
    columns: [
        ["brazil", 60],
        ["france", 10],
        ["united_states", 5],
      ],
    type: 'donut',
    colors: {
        brazil: 'red',
        france: 'green',
        united_states: 'pink',
    }
  };
  
class MyChart extends React.Component {
 
  render() {

    console.log(Chart)

    return (
      <div className="searchPage">
          <h1>myChart</h1>
          <Chart data={data} />
      </div>
    );
  }
}
export default MyChart;