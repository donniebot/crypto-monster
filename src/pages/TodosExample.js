import React, {Component} from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import TodoForm from './Todos/TodoForm'
import TodoList from './Todos/TodoList'
import Preview from '../components/molecules/Preview'
import Flexbox from 'flexbox-react'
import {updateCurrent, currentTodo, addTodo, getTodos} from '../reducers/todo'


class todosExample extends Component {
  constructor(props){
    super();
    this.state = {      
    }
    this.bodyClick = this.bodyClick.bind(this)
  }

  bodyClick(){

    this.setState({

    })

  }

  updateSearchWord(e){
    let searchWord = (e.target.value).toLowerCase()
    let imageData = this.state.data
    let found = []
    if(searchWord){
      let regExp = new RegExp(`^${searchWord}{1}`)
    
      imageData.map(function(image){

        let imageTitle = image.title.toLowerCase()

        if(Boolean(imageTitle.match(regExp))){
          found.push(image)
        }

      });
    }
    this.setState({
      searchResults:found
    });
  }

  render() {

    return (
      <div className="home" onClick={this.bodyClick}>
        <Flexbox flexDirection="column" justifyContent="center" display="flex" height="70%"> 
          <div className="App-header">
            <h2>Todos</h2>
          </div>
          <div className="Todo-App">
            <TodoForm
              currentTodo={this.props.currentTodo}
              updateCurrent={this.props.updateCurrent}
              addTodo={this.props.addTodo}
              todos={this.props.todos}
              />
            <TodoList todos={this.props.todos} />
          </div>
        </Flexbox>
      </div>
    )
  }
}

export default connect(
  (state) => state,
  {updateCurrent, addTodo}
)(todosExample)