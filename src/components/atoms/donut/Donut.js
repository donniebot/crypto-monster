import React from 'react';
import { Link } from 'react-router';
import Chart from 'c3js-react'

const data = {
    columns: [
        ['brazil', 60],
        ['france', 10],
        ['united_states', 5],
      ],
    type: 'donut',
    colors: {
        brazil: 'red',
        france: 'green',
        united_states: 'pink',
    }
  };
  
class Donut extends React.Component {
 
  render() {

    return (
      <div className="searchPage">
          <Chart data={data} />
      </div>
    );
  }
}
export default Donut;