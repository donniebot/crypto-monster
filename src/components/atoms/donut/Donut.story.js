import React from 'react'
import { storiesOf } from '@storybook/react'
import Donut from '.'

const itemClick = () => {
  console.log('clicked')
}

const mouseLeave = () => {
  console.log("mouse leave")
}

const rollOver = () => {
  console.log("rollover")
}

const props = {
  result: {
    action: "Slide 3 Image Action",
    actionHref: "href",
    id: "slide3",
    imageAlt: "Samurai after mods",
    imagePath: "../images/slide3.jpg",
    subtitle: "Slide 3 Image SubTitle",
    text: "Slide 3 Image Text",
    title: "Samurai after mods"
  },
  onRollOverResults: rollOver,
  onItemMouseLeave: mouseLeave,
  previewItemClick: itemClick
}


const story = storiesOf('atoms/Donut', module)
  .add('first', () => {
    return (
      <Donut
        {...props}
      >
      </Donut>
    )
  })

export default story
