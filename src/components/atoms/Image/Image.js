import React, {Component} from 'react'

const Images = ({
  className,
  source
}) => {

  return (
      <div >
        <img className={className} src={source}/>
      </div>
  )
}

export default Images