import React from 'react'
import { storiesOf } from '@storybook/react'
import Image from '.'

const source = 'http://localhost:5000/images/slide2.jpg'

const story = storiesOf('atoms/Image', module)
  .add('Image', () => {
    return (
      <Image
        source={source}>
      </Image>
    )
  })

export default story
