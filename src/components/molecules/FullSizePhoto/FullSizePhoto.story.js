import React from 'react'
import { storiesOf } from '@storybook/react'
import FullSizePhoto from '.'

const source = 'http://localhost:5000/images/slide2.jpg'

const story = storiesOf('atoms/FullSizePhoto', module)
  .add('Display full size photo', () => {
    return (
      <FullSizePhoto 
        source={source}>
      </FullSizePhoto>
    )
  })

export default story
