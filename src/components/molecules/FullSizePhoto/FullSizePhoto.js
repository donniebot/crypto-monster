import React, {Component} from 'react'
import { Link } from 'react-router'
import Image from '../../atoms/Image'

 
const FullSizePhoto = ({
  className,
  source
}) => {
  
    return (
      <div className='searchPage'>
          <h1>FullSizePhoto</h1>
          <Image className='className' source={source}/>
      </div>
    )

}
export default FullSizePhoto;