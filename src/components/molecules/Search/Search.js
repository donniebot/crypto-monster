import React, {Component} from 'react';
import AutoResults from '../AutoResults';

const Search = (props) => {
  const {currentSearch, addPhoto, updateCurrentSearchWord, photos, updatePreviewList} = props
  console.log("props",props)
  let val = '';
  const onChange = (evt) => {
    val = evt.target.value
    updateCurrentSearchWord(val)
    updatePreviewList(val)
  }

  const handleSubmit = (evt) => {
   // const payload = {id:photos.length+1, name: currentSearch, isComplete: true}
   // addPhoto(payload)
    evt.preventDefault()
  }

  let randomName = 's' + Math.random()
  return (
    <div className="search">
      <form onSubmit={handleSubmit}>
        <input className="search-word" name={randomName} type="text" value={currentSearch} onChange={onChange} />
      </form>
    </div>
  )
}

export default Search;