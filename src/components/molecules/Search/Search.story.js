import React from 'react'
import { storiesOf } from '@storybook/react'
import Search from '.'

const updateSearchWord = () => {
    console.log("updateSearchWord")
  }
  

const imageUrl = 'http://localhost:5000/images/slide2.jpg'

const story = storiesOf('molecules/Search', module)
  .add('Input Box', () => {
    return (
      <Search 
        updateSearchWord={updateSearchWord}
        searchWord=''>
      </Search>
    )
  })

export default story
