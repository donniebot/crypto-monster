import React from 'react'
import { storiesOf } from '@storybook/react'
import Preview from './Preview';

const imageUrl = 'http://localhost:5000/images/slide2.jpg'

const story = storiesOf('molecules/Preview', module)
  .add('preview', () => {
    return (
        <Preview previewPhoto={imageUrl}></Preview>
    )
  })

export default story
