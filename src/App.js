import React from 'react'
import { Router, Route, IndexRoute, Link, hashHistory } from 'react-router'

import Home from './pages/Home'
import Layout from './pages/Layout'
import DisplayPhoto from './pages/search/DisplayPhoto'
import TodosExample from './pages/TodosExample'
import MyChart from './pages/MyChart'
import RateMe from './pages/RateMe'
import { subscribeToTimer } from './api';


class App extends React.Component {

    render(){
        let myState = this.props;
        return (
            <div>
                <Router history={ hashHistory }>
                    <Route path="/" component={Layout}>
                        <IndexRoute component={MyChart}></IndexRoute>
                        <Route path="MyChart" component={MyChart}></Route>
                        <Route path="home" component={Home}></Route>
                        <Route path="RateMe" component={RateMe}></Route>
                        <Route path="todosExample" component={TodosExample}></Route>
                        <Route path="displayPhoto/:photoId" component={DisplayPhoto}></Route>
                    </Route> 
                </Router>
            </div>
        );
    }
}
export default App;