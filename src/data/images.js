
const data = [
    {
        id         : "slide1",
        imagePath  : "../images/slide1.jpg",
        imageAlt   : "Slide 1 Image",
        title      : "Charger - my 1968",
        subtitle   : "Slide 1 Image SubTitle",
        text       : "Slide 1 Image Text",
        action     : "Slide 1 Image Action",
        actionHref : "href"
    },
    {
        id         : "slide2",
        imagePath  : "../images/slide2.jpg",
        imageAlt   : "Samurai before mods",
        title      : "Samurai before mods",
        subtitle   : "Slide 2 Image SubTitle",
        text       : "Slide 2 Image Text",
        action     : "Slide 2 Image Action",
        actionHref : "href"
    },
    {
        id         : "slide3",
        imagePath  : "../images/slide3.jpg",
        imageAlt   : "Samurai after mods",
        title      : "Samurai after mods",
        subtitle   : "Slide 3 Image SubTitle",
        text       : "Slide 3 Image Text",
        action     : "Slide 3 Image Action",
        actionHref : "href"
    }
];

export default data;
