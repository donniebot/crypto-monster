const initState = {
  todos: [
    {id:1, name: 'create a store', isComplete: true},
    {id:2, name: 'Load state through the store', isComplete: true},
    {id:3, name: 'Handle state changes with redux', isComplete: false}
  ],
  currentTodo: 'sddsss'
}

const TODO_ADD = 'TODO_ADD'
const CURRENT_UPDATE = 'CURRENT_UPDATE'
const GET_TODOS = 'GET_TODOS'

export const updateCurrent = (val) => ({type:CURRENT_UPDATE, payload: val})

export const addTodo = (val) => ({type:TODO_ADD, payload: val})

export const getTodos = () =>  ({type:GET_TODOS})


export default (state = initState, action) => {
  switch (action.type) {
    case TODO_ADD:
      console.log("TODO_ADD")
      return {...state, todos: state.todos.concat(action.payload)}
    case CURRENT_UPDATE:
      console.log("CURRENT_UPDATE")
      return {...state, currentTodo: action.payload}
    case GET_TODOS:
      console.log("GET_TODOS")
      return state
    default:
      console.log("DEFAULT")
      return state
  }
}
